<?php require_once 'header.php' ?>
<body>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="mt-5 mb-3 clearfix">
                    <h3 class="pull-left">Job position information</h3>
                </div>
                <?php
                if ($jobPosition) {
                    echo '<div class="wrapper">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <p><b>' . $jobPosition->getName() . '</b></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Salary</label>
                                        <p><b>' . $jobPosition->getSalary() . '</b></p>
                                    </div>
                                    <p><a href="/job-position/list" class="btn btn-primary">Back</a></p>
                                </div>
                            </div>
                        </div>
                    </div>';
                } else {
                    echo '<div class="alert alert-danger">
                        <em>Oops! Something went wrong. Please try again later.</em>
                      </div>';
                }
                ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>