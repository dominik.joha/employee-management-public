<?php

use Model\Entity\JobPosition;

require_once 'header.php' ?>
<body>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="mt-5 mb-3 clearfix">
                    <h3 class="pull-left">Job positions list</h3>
                    <a href="/job-position/create"
                       class="btn btn-success btn-sm pull-right">
                        <i class="fa fa-plus"></i> add new job position
                    </a>
                </div>
                <?php
                if ($jobPositions !== null) {
                    echo '<table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Salary</th>
                    <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>';
                    /** @var JobPosition $jobPosition */
                    foreach ($jobPositions as $jobPosition) {
                        echo '<tr>
                                <td>' . $jobPosition->getPrimary() . '</td>
                                <td>' . $jobPosition->getName() . '</td>
                                <td>' . $jobPosition->getSalary() . '</td>
                                <td>
                                    <a href="/job-position/show?id=' . $jobPosition->getPrimary() . '" class="mr-3" title="View Record" data-toggle="tooltip"><span class="fa fa-eye"></span></a>
                                    <a href="/job-position/edit?id=' . $jobPosition->getPrimary() . '" class="mr-3" title="Update Record" data-toggle="tooltip"><span class="fa fa-pencil"></span></a>
                                    <a href="/job-position/delete?id=' . $jobPosition->getPrimary() . '" title="Delete Record" data-toggle="tooltip"><span class="fa fa-trash"></span></a>
                                </td>
                            </tr>';
                    }
                    echo "</tbody>
                         </table>";
                } else {
                    echo '<div class="alert alert-danger"><em>No records were found.</em></div>';
                }
                ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>