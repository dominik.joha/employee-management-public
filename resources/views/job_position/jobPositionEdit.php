<?php

use Model\Entity\JobPosition;

require_once 'header.php';

/** @var JobPosition|null $jobPosition */
?>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="mt-5">Edit job position</h3>
                    <form action="/job-position/edit?id=<?php echo $jobPosition->getPrimary(); ?>" method="post">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control"
                                   value="<?php echo $jobPosition->getName(); ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Salary</label>
                            <input type="number" name="salary" class="form-control"
                                   value="<?php echo $jobPosition->getSalary(); ?>" required>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="/job-position/list" class="btn btn-secondary ml-2">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>