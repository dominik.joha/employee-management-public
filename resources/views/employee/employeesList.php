<?php

require_once 'header.php' ?>
<body>
<div class="wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="mt-5 mb-3 clearfix">
                <h3 class="pull-left">Employees list</h3>
                <a href="/employee/create"
                   class="btn btn-success btn-sm pull-right"><i
                   class="fa fa-plus"></i> add new employee
                </a>
            </div>
            <?php
            if ($employees !== null) {
                echo '<table class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>Name</th>
                <th>Surname</th>
                <th>Email</th>
                <th>Job position</th>
                <th>Salary</th>
                <th>Action</th>
                </tr>
              </thead>
              <tbody>';
                foreach ($employees as $employee) {
                    echo '<tr>
                            <td>' . $employee['name'] . '</td>
                            <td>' . $employee['surname'] . '</td>
                            <td>' . $employee['email'] . '</td>
                            <td>' . $employee['job_position_name'] . '</td>
                            <td>' . $employee['salary'] . '</td>
                            <td>
                                <a href="/employee/show?id=' . $employee['id'] . '"
                                    class="mr-3" title="View Record" 
                                    data-toggle="tooltip">
                                    <span class="fa fa-eye"></span>
                                </a>
                                <a href="/employee/edit?id=' . $employee['id'] . '"
                                    class="mr-3" title="Update Record"
                                    data-toggle="tooltip">
                                    <span class="fa fa-pencil"></span>
                                </a>
                                <a href="/employee/delete?id=' . $employee['id'] . '" 
                                    title="Delete Record" data-toggle="tooltip">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </td>
                        </tr>';
                }
                echo "</tbody>
                     </table>";
            } else {
                echo '<div class="alert alert-danger"><em>No records were found.</em></div>';
            }
            ?>
        </div>
    </div>
</div>
</body>
</html>