<?php

use Model\Entity\JobPosition;

require_once 'header.php';
if ($jobPositions) {
?>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="mt-5">Create Record</h3>
                    <p>Please fill this form and submit to add employee.</p>
                    <form action="/employee/create" method="post">
                        <div class="form-group">
                            <label>Job position</label>
                            <select class="form-control" name="job_position_id" id="job_position_id">
                                <?php
                                /** @var JobPosition|null $jobPosition */
                                foreach ($jobPositions as $jobPosition) {
                                    echo '<option value ="' . $jobPosition->getPrimary() . '" >
                                        ' . $jobPosition->getName() . '
                                        </option >';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Surname</label>
                            <input type="text" name="surname" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Degree</label>
                            <input type="text" name="degree" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="number" name="phone" class="form-control" required>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="/employee/list" class="btn btn-secondary ml-2">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
    <?php
} else {
    echo ' You must first create a job position 
        <a href="/job-position/create" class="btn btn-secondary ml-2">create job position here </a>';
}
?>