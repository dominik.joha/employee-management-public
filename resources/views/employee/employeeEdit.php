<?php

use Model\Entity\JobPosition;
use Model\Entity\Employee;

require_once 'header.php';

/** @var Employee $employee */
$employee
?>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="mt-5">Edit employee</h3>
                <form action="/employee/edit?id=<?php echo $employee->getPrimary() ?>" method="post">
                    <div class="form-group">
                        <label>Job position</label>
                        <select class="form-control" name="job_position_id" id="job_position_id">
                            <?php
                            /** @var JobPosition|null $jobPosition */
                            foreach ($jobPositions as $jobPosition) {
                                echo ($jobPosition->getPrimary() === $employee->getPositionId()) ?
                                    "<option selected='selected' 
                                            value='" . $jobPosition->getPrimary() . "'>" . $jobPosition->getName() .
                                    "</option>"
                                    :
                                    "<option
                                            value='" . $jobPosition->getPrimary() . "'>" . $jobPosition->getName() .
                                    "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Name</label>
                        <input
                                type="text"
                                value="<?php echo $employee->getName(); ?>"
                                name="name"
                                class="form-control"
                                required
                        >
                    </div>
                    <div class="form-group">
                        <label>Surname</label>
                        <input type="text" value="<?php echo $employee->getSurname(); ?>"
                               name="surname" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Degree</label>
                        <input type="text" value="<?php echo $employee->getDegree(); ?>"
                               name="degree" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" value="<?php echo $employee->getEmail(); ?>"
                               name="email" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <input type="number" value="<?php echo $employee->getPhone(); ?>"
                               name="phone" class="form-control" required>
                    </div>
                    <input type="submit" class="btn btn-primary" value="Submit">
                    <a href="/employee/list" class="btn btn-secondary ml-2">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>
