<?php

use Model\Entity\Employee;

require_once 'header.php' ?>
<body>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="mt-5 mb-3 clearfix">
                    <h3 class="pull-left">Employee Information</h3>
                </div>
                <?php
                /** @var Employee|null $employee */
                if ($employee !== null) {
                    echo '<div class="wrapper">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <p><b>' . $employee->getName() . '</b></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Surname</label>
                                        <p><b>' . $employee->getSurname() . '</b></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Degree</label>
                                        <p><b>' . $employee->getDegree() . '</b></p>
                                    </div>
                                    <div class="form-group">
                                        <label>email</label>
                                        <p><b>' . $employee->getEmail() . '</b></p>
                                    </div>
                                    <div class="form-group">
                                        <label>phone</label>
                                        <p><b>' . $employee->getPhone() . '</b></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Job position</label>
                                        <p><b>' . $employee->getJobPosition()->getName() . '</b></p>
                                    </div>  
                                    <div class="form-group">
                                        <label>Salary</label>
                                        <p><b>' . $employee->getJobPosition()->getSalary() . '</b></p>
                                    </div>  
                                    <p><a href="/employee/list" class="btn btn-primary">Back</a></p>
                                </div>
                            </div>
                        </div>
                    </div>';
                } else {
                    echo '<div class="alert alert-danger">
                        <em>Oops! Something went wrong. Please try again later.</em>
                      </div>';
                }
                ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>