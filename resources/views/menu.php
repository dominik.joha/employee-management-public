<?php $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH); ?>
<div class="wrapper">
    <div class="mt-5 mb-3 clearfix">
        <h2 class="pull-left">Employees management</h2>
    </div>
</div>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item <?php echo $uri === '/' || $uri === '/index.php' ? 'active' : '' ?>">
                <a class="nav-link" href="/">Home</a>
            </li>
            <li class="nav-item <?php echo str_contains($uri, '/job-position') ? 'active' : '' ?>">
                <a class="nav-link"
                   href="/job-position/list">Job position</a>
            </li>
            <li class="nav-item <?php echo str_contains($uri, '/employee/') ? 'active' : '' ?>">
                <a class="nav-link"
                   href="/employee/list">Employees</a>
            </li>
        </ul>
    </div>
</nav>