<?php

require_once '../../autoload.php';

$requestUri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

if ($requestUri === '/' || $requestUri === '/index.php') {
    require_once 'dashboard.php';
    exit();
}

$uriSegment = explode('/', $requestUri);
$requestController = "Controllers\\" . ucfirst(str_replace(' ', '', ucwords(str_replace('-', ' ', $uriSegment[1])))) . "Controller";
$requestMethod = 'action' . ucfirst($uriSegment[2]);

try {
    if (class_exists($requestController) && method_exists($requestController, $requestMethod)) {
        $controller = new $requestController();
        if (isset($_GET['id']) && is_numeric($_GET['id'])) {
            $controller->$requestMethod($_GET['id']);
            $response = $controller->$requestMethod($_GET['id']);
            echo $response->render();
            exit();
        }
        $response = $controller->$requestMethod();
        echo $response->render();
        exit();
    }
    throw new LogicException('Class or Method not exist');
} catch (Throwable $e) {
    require_once 'error.php';
}
