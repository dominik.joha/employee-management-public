<?php

namespace Model\Repository;

use Exception;
use Model\Entity\JobPosition;

class JobPositionRepository extends AbstractRepository
{
    public function create(JobPosition $jobPosition): bool
    {
        return $this->pdo->prepare(
            'INSERT INTO  ' . $this->getTable() . '
             (name, salary, created_at) VALUES (?, ?, ?)'
        )->execute([
            $jobPosition->getName(),
            $jobPosition->getSalary(),
            $jobPosition->getCreatedAt()->format("Y-m-d H:i:s")
        ]);
    }

    public function update(JobPosition $jobPosition): bool
    {
        return $this->pdo->prepare(
            'UPDATE  ' . $this->getTable() . ' SET 
             name=?, salary=? WHERE id=?'
        )->execute([
            $jobPosition->getName(),
            $jobPosition->getSalary(),
            $jobPosition->getPrimary()
        ]);
    }

    protected function getEntityClass(): string
    {
        return JobPosition::class;
    }

    protected function getTable(): string
    {
        return 'job_positions';
    }

    /**
     * @throws Exception
     */
    protected function createFromArray(array $row): JobPosition
    {
        return JobPosition::createFromArray($row);
    }
}
