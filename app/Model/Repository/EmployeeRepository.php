<?php

namespace Model\Repository;

use Exception;
use Model\Entity\Employee;
use PDO;

class EmployeeRepository extends AbstractRepository
{
    private const JOB_POSITIONS_TABLE = 'job_positions';

    public function create(Employee $employee): bool
    {
        return $this->pdo->prepare(
            'INSERT INTO  ' . $this->getTable() . '
             (position_id, name, surname, degree, email, phone, created_at) VALUES (?, ?, ?, ?, ?, ?, ?)'
        )->execute([
            $employee->getPositionId(),
            $employee->getName(),
            $employee->getSurname(),
            $employee->getDegree(),
            $employee->getEmail(),
            $employee->getPhone(),
            $employee->getCreatedAt()->format("Y-m-d H:i:s")
        ]);
    }

    public function getAllEmployeesWithJobPosition(): ?array
    {
        $result = $this->pdo->query(
            "SELECT
                        e.id,
                        e.name,
                        e.surname,
                        e.degree,
                        e.email,
                        e.phone,
                        jp.name as job_position_name,
                        jp.salary              
            FROM " . $this->getTable() . " e
            JOIN " . self::JOB_POSITIONS_TABLE . " jp ON (e.position_id = jp.id)"
        )->fetchAll(PDO::FETCH_ASSOC);

        if (empty($result)) {
            return null;
        }

        return $result;
    }

    public function update(Employee $employee): bool
    {
        return $this->pdo->prepare(
            'UPDATE  ' . $this->getTable() . ' SET 
             position_id=?, name=?, surname=?, degree=?, email=?, phone=? WHERE id=?'
        )->execute([
            $employee->getPositionId(),
            $employee->getName(),
            $employee->getSurname(),
            $employee->getDegree(),
            $employee->getEmail(),
            $employee->getPhone(),
            $employee->getPrimary(),
        ]);
    }

    protected function getEntityClass(): string
    {
        return Employee::class;
    }

    protected function getTable(): string
    {
        return 'employees';
    }


    /**
     * @throws Exception
     */
    protected function createFromArray(array $row): Employee
    {
        return Employee::createFromArray($row);
    }
}
