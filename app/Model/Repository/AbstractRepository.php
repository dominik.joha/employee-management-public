<?php

namespace Model\Repository;

use Model\Entity\EntityInterface;
use PDO;
use Service\DB\DbConnector;

abstract class AbstractRepository
{
    protected PDO $pdo;

    abstract protected function getEntityClass(): string;

    abstract protected function getTable(): string;

    abstract protected function createFromArray(array $row): ?EntityInterface;

    public function __construct()
    {
        $instance = DbConnector::getInstance();
        $this->pdo = $instance->getConnection();
    }

    public function findById(int $id): ?EntityInterface
    {
        $sqlQuery = "SELECT *
                      FROM
                        " . $this->getTable() . "
                    WHERE 
                       id = ?
                    LIMIT 0,1";
        $stmt = $this->pdo->prepare($sqlQuery);
        $stmt->bindParam(1, $id);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);


        if (false === $result) {
            return null;
        }

        return $this->createFromArray($result);
    }

    public function findAll(): ?array
    {
        $result = $this->pdo->query(
            "SELECT * FROM " . $this->getTable()
        )->fetchAll(PDO::FETCH_ASSOC);

        if (empty($result)) {
            return null;
        }

        $arrayOfEntities = [];
        foreach ($result as $item) {
            $arrayOfEntities[] = $this->createFromArray($item);
        }
        return $arrayOfEntities;
    }

    public function deleteById(int $id): bool
    {
        return $this->pdo->prepare(
            "DELETE FROM " . $this->getTable() . " 
            WHERE id = ?"
        )->execute([$id]);
    }
}
