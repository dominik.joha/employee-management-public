<?php

namespace Model\Entity;

use DateTimeImmutable;
use Exception;

class JobPosition implements EntityInterface
{
    private function __construct(
        private string $name,
        private float $salary,
        private readonly DateTimeImmutable $createdAt,
        private readonly ?int $id,
    ) {
    }

    /**
     * @throws Exception
     */
    public static function createFromArray(array $row): JobPosition
    {
        return new self(
            $row['name'],
            $row['salary'],
            new DateTimeImmutable($row['created_at']),
            $row['id']
        );
    }

    public static function createFromPostData(array $postData): JobPosition
    {
        return new self(
            trim($postData['name']),
            trim($postData['salary']),
            new DateTimeImmutable(),
            null
        );
    }

    public function updateAfterPostForm(array $postData): void
    {
        $this->name = trim($postData["name"]);
        $this->salary = trim($postData["salary"]);
    }

    public function getPrimary(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSalary(): ?float
    {
        return $this->salary;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }
}
