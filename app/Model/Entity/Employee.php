<?php

namespace Model\Entity;

use DateTimeImmutable;
use Exception;

class Employee implements EntityInterface
{
    private function __construct(
        private int $positionId,
        private string $name,
        private string $surname,
        private string $degree,
        private string $email,
        private string $phone,
        private readonly DateTimeImmutable $createdAt,
        private readonly ?int $id,
        private ?JobPosition $jobPosition
    ) {
    }

    /**
     * @throws Exception
     */
    public static function createFromArray(array $row): Employee
    {
        return new self(
            $row['position_id'],
            $row['name'],
            $row['surname'],
            $row['degree'],
            $row['email'],
            $row['phone'],
            new DateTimeImmutable($row['created_at']),
            $row['id'],
            null
        );
    }

    public static function createFromPostData(array $postData): Employee
    {
        return new self(
            trim($postData["job_position_id"]),
            trim($postData["name"]),
            trim($postData["surname"]),
            trim($postData["degree"]),
            trim($postData["email"]),
            trim($postData["phone"]),
            new DateTimeImmutable(),
            null,
            null
        );
    }

    public function updateAfterPostForm(array $postData): void
    {
        $this->positionId = trim($postData["job_position_id"]);
        $this->name = trim($postData["name"]);
        $this->surname = trim($postData["surname"]);
        $this->degree = trim($postData["degree"]);
        $this->email = trim($postData["email"]);
        $this->phone = trim($postData["phone"]);
    }

    public function getPrimary(): int
    {
        return $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPositionId(): int
    {
        return $this->positionId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function getDegree(): string
    {
        return $this->degree;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getJobPosition(): JobPosition
    {
        return $this->jobPosition;
    }

    public function setJobPosition(JobPosition $jobPosition): void
    {
        $this->jobPosition = $jobPosition;
    }
}
