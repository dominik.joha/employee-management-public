<?php

namespace Model\Entity;

interface EntityInterface
{
    public static function createFromArray(array $row): EntityInterface;

    public function getPrimary(): int;
}
