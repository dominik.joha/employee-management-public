<?php

namespace Service\Response;

class ViewResponse implements ResponseInterface
{
    public function __construct(
        private readonly string $view,
        private array $data = []
    ) {
    }

    public function render(): string|bool
    {
        extract($this->data);
        ob_start();
        require_once '../../resources/views/' . $this->view . '.php';
        return ob_get_clean();
    }
}
