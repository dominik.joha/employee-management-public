<?php

namespace Service\Response;

class RedirectResponse implements ResponseInterface
{
    public function __construct(
        private readonly string $url
    ) {
    }

    public function render(): void
    {
        header(sprintf('location: %s', $this->url));
    }
}
