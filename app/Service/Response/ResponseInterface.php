<?php

namespace Service\Response;

interface ResponseInterface
{
    public function render();
}
