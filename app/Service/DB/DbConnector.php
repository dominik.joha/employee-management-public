<?php

namespace Service\DB;

use PDO;
use Throwable;

class DbConnector
{
    private PDO $connection;

    public static $dbConnector;

    private function __construct()
    {
        try {
            $this->connection = new PDO(
                "mysql:host=" . getenv('MYSQL_HOST') . ";dbname=" . getenv('MYSQL_DATABASE'),
                getenv('MYSQL_USER'),
                getenv('MYSQL_PASSWORD')
            );
            $this->connection->exec("set names utf8");
        } catch (Throwable $e) {
            echo "Database could not be connected: " . $e->getMessage();
        }
    }

    public static function getInstance(): DbConnector
    {
        if (!self::$dbConnector) {
            self::$dbConnector = new self();
        }

        return self::$dbConnector;
    }

    public function getConnection(): PDO
    {
        return $this->connection;
    }
}
