<?php

namespace Controllers;

use Model\Entity\Employee;
use model\Entity\JobPosition;
use Model\Repository\EmployeeRepository;
use Model\Repository\JobPositionRepository;
use Service\Response\RedirectResponse;
use Service\Response\ResponseInterface;
use Service\Response\ViewResponse;

class EmployeeController
{
    private EmployeeRepository $employees;

    private JobPositionRepository $jobPositions;

    public function __construct()
    {
        $this->employees = new EmployeeRepository();
        $this->jobPositions = new JobPositionRepository();
    }

    public function actionList(): ViewResponse
    {
        $employees = $this->employees->getAllEmployeesWithJobPosition();
        return new ViewResponse('employee/employeesList', ['employees' => $employees]);
    }

    public function actionCreate(): ResponseInterface
    {
        $jobPositions = $this->jobPositions->findAll();
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            $this->employees->create(
                Employee::createFromPostData($_POST)
            );
            return new RedirectResponse('/employee/list');
        }
        return new ViewResponse('employee/employeeCreate', ['jobPositions' => $jobPositions]);
    }

    public function actionShow($id): ViewResponse
    {
        /** @var Employee|null $employee */
        $employee = $this->employees->findById($id);

        if ($employee !== null) {
            /** @var JobPosition|null $jobPosition */
            $jobPosition = $this->jobPositions->findById($employee->getPositionId());

            if ($jobPosition !== null) {
                $employee->setJobPosition($jobPosition);
            }
        }
        return new ViewResponse('employee/employeeShow', ['employee' => $employee]);
    }

    public function actionEdit(int $id): ResponseInterface
    {
        $jobPositions = $this->jobPositions->findAll();
        /** @var Employee|null $employee */
        $employee = $this->employees->findById($id);

        if (($employee !== null) && $_SERVER["REQUEST_METHOD"] === "POST") {
            $employee->updateAfterPostForm($_POST);
            $this->employees->update($employee);
            return new RedirectResponse('/employee/list');
        }
        return new ViewResponse(
            'employee/employeeEdit',
            [
                'employee' => $employee,
                'jobPositions' => $jobPositions
            ]
        );
    }

    public function actionDelete(int $id): RedirectResponse
    {
        $this->employees->deleteById($id);
        return new RedirectResponse('/employee/list');
    }
}
