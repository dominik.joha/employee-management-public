<?php

namespace Controllers;

use Model\Entity\JobPosition;
use Model\Repository\JobPositionRepository;
use Service\Response\RedirectResponse;
use Service\Response\ResponseInterface;
use Service\Response\ViewResponse;

class JobPositionController
{
    /**
     * @var JobPositionRepository
     */
    private JobPositionRepository $jobPositions;

    public function __construct()
    {
        $this->jobPositions = new JobPositionRepository();
    }

    public function actionList(): ViewResponse
    {
        $jobPositions = $this->jobPositions->findAll();
        return new ViewResponse('job_position/jobPositionsList', ['jobPositions' => $jobPositions]);
    }

    public function actionCreate(): ResponseInterface
    {
        if (
            $_SERVER["REQUEST_METHOD"] === "POST" &&
            $this->jobPositions->create(JobPosition::createFromPostData($_POST))
        ) {
            return new RedirectResponse('/job-position/list');
        }
        return new ViewResponse('job_position/jobPositionCreate');
    }

    public function actionShow($id): ViewResponse
    {
        /** @var JobPosition|null $jobPosition */
        $jobPosition = $this->jobPositions->findById($id);
        return new ViewResponse('job_position/jobPositionShow', ['jobPosition' => $jobPosition]);
    }

    public function actionEdit(int $id): ResponseInterface
    {
        /** @var JobPosition|null $jobPosition */
        $jobPosition = $this->jobPositions->findById($id);
        if (($jobPosition !== null) && $_SERVER["REQUEST_METHOD"] === "POST") {
            $jobPosition->updateAfterPostForm($_POST);
            $this->jobPositions->update($jobPosition);
            return new RedirectResponse('/job-position/list');
        }
        return new ViewResponse('job_position/jobPositionEdit', ['jobPosition' => $jobPosition]);
    }

    public function actionDelete(int $id): RedirectResponse
    {
        $this->jobPositions->deleteById($id);
        return new RedirectResponse('/job-position/list');
    }
}
