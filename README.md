## Install locale

### Add hostname to hosts file

Add your new entries:
```   
    127.0.0.1       employeemanagement.local
```
### Docker

Docker build - using the command below:
```
    docker-compose -f docker-compose.yml up -d --build
```