USE employee_management;

CREATE TABLE IF NOT EXISTS job_positions
(
    id         INT AUTO_INCREMENT,
    name       VARCHAR(255) DEFAULT NULL,
    salary     FLOAT    NOT NULL,
    created_at DATETIME NOT NULL,
    INDEX IDX_NAME (name),
    PRIMARY KEY (id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS employees
(
    id          INT AUTO_INCREMENT,
    position_id INT          DEFAULT NULL,
    name        VARCHAR(30)  DEFAULT NULL,
    surname     VARCHAR(30)  DEFAULT NULL,
    degree      VARCHAR(30)  DEFAULT NULL,
    email       VARCHAR(255) DEFAULT NULL,
    phone       VARCHAR(15) NOT NULL,
    created_at  DATETIME    NOT NULL,
    INDEX IDX_POSITION_ID (position_id),
    INDEX IDX_NAME (name),
    INDEX IDX_SURNAME (surname),
    INDEX IDX_DEGREE (degree),
    INDEX IDX_EMAIL (email),
    PRIMARY KEY (id),
    CONSTRAINT FK_POSITION_ID FOREIGN KEY (position_id) REFERENCES job_positions (id) ON DELETE CASCADE
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;